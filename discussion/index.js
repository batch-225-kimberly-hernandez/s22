// Array Methods - Javascript has built-in functions and methods for arrays. This allows us to manipulate and access array items.

// Mutator Methods
/*
    - Mutator methods are functions that "mutate" or change an array after they're created.
    - These methods manipulate the original array performing various task such as adding and removing elements.
*/


// push()
/*
    - Adds an element in the end of an array And return the array's length.

    Syntax:
        arrayName.push();

*/

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit',];

console.log('Current array:');
console.log(fruits);

let fruitsLength = fruits.push('Mango');
// console.log(fruitsLength);
console.log('Mutated array from push method:');
console.log(fruits);

// Adding multiple elements to an array
fruits.push('Avocado', 'Guava');
console.log('Mutated array from push method:');
console.log(fruits);


// pop() 
/*
    - Removes the last element in an array AND returns the removed element.
    - Syntax
        arrayName.pop();
*/

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log('Mutated array from pop method:');
console.log(fruits);


// unshift()
/*
    - Adds one or more elements at the beginning of an array.
    Syntax:

        arrayName.unshift('elementA');
        arrayName.unshift('elementA', elementB);
*/

fruits.unshift('Lime', 'Banana');
console.log('Mutated array from unshift method:');
console.log(fruits);


// Shift()

/*
    - Removes an element at the beginning of an array and Returns the removed element
    - Syntax
        arrayName.shift();
*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log('Mutated array from shift method:');
console.log(fruits);

// splice()
/*
    - Simultaneously removes elements from a specified index number and adds element
    - Syntax
        arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);
*/

fruits.splice(1, 2, 'Lime');
// console.log(try1)
console.log('Mutated array from splice method:');
console.log(fruits);


// sort()
/* 
    - Rearrange the array elements in alphanumeric order.
    - Syntax:
        arrayName.sort();
*/

const randomThings = ['Cat', 'boy', 'apps', 'zoo']

randomThings.sort()
console.log('Mutated array from sort method:')
console.log(randomThings)

// reverse()

/* 
    - Reverse the order of array elements
    - Syntax
        arrayName.reverse()
*/

randomThings.reverse()
console.log('Mutated array from reverse method:')
console.log(randomThings)

// Non-Mutator Methods
/*
    - Non-mutator methods are functions that do not modify or change and array after they're created.
    - These methods do not manipulate the original array performing various tasks such as returning elements from an array and combining array and printing the output.
*/

// indexOf()

/* 
    - Returns the index number of the first matching element found in an array.
    - If there is no match, the result will be "-1".
    - The search process will be done from first element proceeding to the last element.

    - Syntax:

            arrayName.indexof(searchValue)
*/

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'BR', 'FR', 'DE']

let firstIndex = countries.indexOf('CAN')

console.log("Result of indexOf method: " + firstIndex)

// slice()

/* 
    - Portion/slices elements from an array AND returns a new array.
    Syntax: 
        arrayName.slice(startingIndex)
        arrayName.slice(startingIndex, endingIndex)

*/

// Slicing off elements from a specified index to the first element

let slicedArrayA = countries.slice(4);
console.log('Result from sliceA method:')
console.log(countries)
console.log(slicedArrayA)


// Slicing off elements from a specified index to another index

// Note: The last element is not included.

let slicedArrayB = countries.slice(2, 4);
console.log('Result from sliceB method')
console.log(slicedArrayB)

// Slicing off elements starting from the last element of an array
let slicedArrayC = countries.slice(-3)
console.log('Result from sliceC method:')
console.log(slicedArrayC)


// forEach();

/* 
    - Similar to a for loop that iterates on each array element.
    - For Each item in the array, the anonymous function passed in forEach() method will be run.
    - arrayName - plural for good practice
    - parameter - singular
    - Note: it must have a function
    - Syntax:
        arrayName.forEach(function(individualElement)
        {
                Statement
        }
        )
*/

countries.forEach(function (country) {
    console.log(country);
})

// includes()

/* 
    includes()

    - includes() method checks if the argument passed can be found in the array.
    - It returns the boolean which can be saved in a variable.
        - return true if the argument can be found in the array
        - return false if the argument cannot be found in the array

            - Syntax:
                arrayName.includes(<argumentToFind>)
*/

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];


let productFound1 = products.includes('Mouse');

console.log(productFound1);//returns true

let productFound2 = products.includes('Headset')

console.log(productFound2);//returns false
